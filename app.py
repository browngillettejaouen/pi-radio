import os
import subprocess
from flask import Flask, redirect, render_template, request, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
path = os.path.join(os.path.expanduser("~/dbs"), 'development.db')
app.config['SQLALCHEMY_DATABASE_URI'] = f"sqlite:///{path}"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)


class Stream(db.Model):
    id = db.Column(
        db.Integer,
        primary_key=True
    )
    name = db.Column(
        db.String(240),
        unique=True,
        nullable=False
    )
    url = db.Column(
        db.String(240),
        unique=True,
        nullable=False
    )
    currently_playing = db.Column(
        db.Boolean,
        unique=False,
        default=False
    )
    created = db.Column(
        db.DateTime(timezone=True),
        server_default=db.func.now()
    )
    updated = db.Column(
        db.DateTime(timezone=True),
        onupdate=db.func.now()
    )

    def __repr__(self):
        return "<Stream %r>" % self.name


@app.route("/")
def index():
    streams = Stream.query.order_by(Stream.name)
    currently_playing = None
    for stream in streams:
        if stream.currently_playing:
            currently_playing = stream
            break

    none_selected = True
    if currently_playing:
        none_selected = False

    return render_template("index.html", streams=streams,
                           currently_playing=currently_playing,
                           none_selected=none_selected)


@app.route("/play", methods=["POST"])
def play():
    if not request.form.get("stream"):
        return redirect(url_for("index"))

    stream_id = int(request.form["stream"])
    stream = Stream.query.get_or_404(stream_id)
    submitted = request.form.get("submit")
    deleted = request.form.get("delete")

    if submitted and not stream.currently_playing:
        streams = Stream.query.all()
        for stream_ in streams:
            stream_.currently_playing = False

        db.session.commit()

        subprocess.call(["mpc", "clear"])
        subprocess.call(["mpc", "add", stream.url])
        subprocess.call(["mpc", "play"])

        stream.currently_playing = True
        db.session.commit()
    elif deleted:
        if stream.currently_playing:
            subprocess.call(["mpc", "clear"])

        db.session.delete(stream)
        db.session.commit()

    return redirect(url_for("index"))


@app.route("/stop", methods=["POST"])
def stop():
    streams = Stream.query.all()
    for stream in streams:
        stream.currently_playing = False

    db.session.commit()
    subprocess.call(["mpc", "clear"])

    return redirect(url_for("index"))


@app.route("/create", methods=["POST"])
def create():
    stream_name = request.form["stream_name"]
    stream_url = request.form["stream_url"]
    stream = Stream(name=stream_name, url=stream_url)
    db.session.add(stream)
    db.session.commit()

    return redirect(url_for("index"))
