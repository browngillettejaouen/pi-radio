* Install MPD and MPC.

  #+BEGIN_SRC bash
  sudo apt install mpd
  sudo apt install mpc
  #+END_SRC

* Install SQLite.

  #+BEGIN_SRC bash
  sudo apt install sqlite
  #+END_SRC

* pip install flask & flask-sqlalchemy

  #+BEGIN_SRC bash
  pip3 install flask flask-sqlalchemy
  #+END_SRC

* Clone down the repository.

  #+BEGIN_SRC bash
  git clone git@bitbucket.org:browngillettejaouen/pi-radio.git
  #+END_SRC

* Create the dbs directory.

  #+BEGIN_SRC bash
  mkdir -p ~/dbs
  #+END_SRC

* Create the db.

  #+BEGIN_SRC python
  from app import db
  db.create_all()
  #+END_SRC

* Run the app.

  #+BEGIN_SRC bash
  FLASK_ENV=development FLASK_APP=app.py flask run --host=0.0.0.0
  #+END_SRC
